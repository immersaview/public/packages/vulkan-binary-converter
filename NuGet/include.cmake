cmake_minimum_required(VERSION 3.0)

set(VKSBC_COMMAND ${CMAKE_CURRENT_LIST_DIR}/vksbc CACHE INTERNAL "VKSBC command path")
message(STATUS "vksbc at ${VKSBC_COMMAND}")