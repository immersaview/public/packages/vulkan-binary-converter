# Vulkan Binary Converter

Fork from the [Vulkan Binary Converter](https://github.com/StableCoder/vksbc) tool, generalised to let us convert any data file into a C header to include in compiled binaries.

